#!/bin/bash

echo "							INSTALL LOLCAT"
yay lolcat 
echo "							INSTALL ROXTERM" | lolcat
yay roxterm
echo "							INSTALL FIGLET" | lolcat
yay figlet 
echo "							INSTALL LIBNEWT" | lolcat
yay libnewt
echo "							INSTALL HWINFO" | lolcat
yay hwinfo 
echo "							INSTALL BASHMOUNT" | lolcat
yay bashmount

echo ""
echo "							DEPS INSTALLED" | lolcat

exit 0

