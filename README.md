# dd-create-live-iso-cli :: Bashmount

# ADDED VENTOY TO script

### location scripts


     /usr/local/bin/start-dd-create-live-iso-cli.sh
     /usr/local/bin/dd-create-live-iso

 ### Run startup script.
    
 `$ start-dd-create-live-iso-cli.sh`  

  - _opens roxterm exec __dd-create-live-iso-cli__ & set roxterm geometry._

 `$ dd-create-live-iso-cli`

  - _opening --tab with bashmount._

  - _list usb devices to choose from. refresh media option._

  - _format usb as ext4, if not allready done._

  - _search for ISO files in the downloads dir with whiptail._

  - _possible to safely unmount device, when the job is done, with bashmount._

  - _close or create new iso._


##### DEPS ::: __roxterm, whiptail, hwinfo, lolcat, figlet, nerd-fonts-hack, bashmount.__

Install [Bashmount Github](https://github.com/jamielinux/bashmount)

---

`Made and tested with Archcraft i3.`

##### For roxterm arch you maybe need todo the folowing.

    $ nano /etc/environment or .bashrc
    
    add : 
    
    export NO_AT_BRIDGE=1
     
##### Logout/in to take effect.

`Tested on Parrot home xfce4 :+:`

`Tested on Mxlinux-Fluxbox 64/32bit :+:`

---

#### SCROT ...

![Screenshot_2022-05-24-21-10-55_1280x800](/uploads/a879b59b10ec850a886e8a32ef8e9300/Screenshot_2022-05-24-21-10-55_1280x800.png)
